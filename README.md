# README #

Docker image running Jupyter Notebook, Conda, Tensorflow etc.
Base: https://github.com/jupyter/docker-stacks // https://hub.docker.com/r/jupyter/scipy-notebook/

Adds TensorFlow (Python 2.7 only atm) and an example jupyter notebook showing a TensorFlow NN in action.

### Usage: ###

Build the  image (about 5GB):
```
#!shell

docker build -t insign/ml .
```

Run the container:
```
#!shell

docker run --name=ml -d -p 8888:8888 -v /<YOUR WORKING DIR>:/home/jovyan/work/volume insign/ml
```

Then go to [localhost:8888](http://localhost:8888) and try the Jupyther notebook iris example.

Note: TensorFlow is only installed for Python 2.7 at the moment.


### Other useful docker commands: ###

Log in to the container:
```
#!shell

docker exec -it ml /bin/bash
```

Stop / (re-) start the container
```
#!shell

docker start | stop ml
```