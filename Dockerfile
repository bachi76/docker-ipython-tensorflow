# See: https://github.com/jupyter/docker-stacks/blob/master/scipy-notebook/Dockerfile
FROM jupyter/scipy-notebook

MAINTAINER Bachi <m.bachmann@insign.ch>

USER jovyan

# Install TensorFlow to Python 2.7 only for now (got issues with 3.5 it seemed)
RUN /bin/bash -c "source activate python2"
RUN /opt/conda/envs/python2/bin/pip install --ignore-installed --upgrade https://storage.googleapis.com/tensorflow/linux/cpu/tensorflow-0.8.0-cp27-none-linux_x86_64.whl

# Our high-level TensorFlow abstraction, Keras: http://keras.io/
# (set to use TensorFlow as its backend)
RUN /opt/conda/envs/python2/bin/pip install --ignore-installed --upgrade keras
ENV KERAS_BACKEND=tensorflow

COPY datasets ./datasets/
COPY examples ./examples/

VOLUME /home/jovyan/work/

# Could be used to run python on the host (e.g. from PyCharm)
# VOLUME /opt/conda


RUN echo "AI installed...\n    ...now run it using: docker run --name=ml -d -p 8888:8888 -v /<YOUR WORKING DIR>:/home/jovyan/work insign/ml"

